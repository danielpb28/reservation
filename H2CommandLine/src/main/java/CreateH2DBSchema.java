import java.sql.*;
import org.h2.jdbcx.JdbcConnectionPool;

public class CreateH2DBSchema {

	public static void main(String[] args) {
		Connection conn = null;
		try {
			JdbcConnectionPool cp = JdbcConnectionPool
					.create("jdbc:h2:~/reservation/db;AUTO_SERVER=TRUE", "sa", "");
			conn = cp.getConnection();
			//conn.createStatement().execute("DROP TABLE RESERVATION;");
			//conn.createStatement()
			//		.execute("CREATE TABLE RESERVATION(ID INT PRIMARY KEY, NAME VARCHAR, DATETIME TIMESTAMP );");
			for (int i = 0; i < 100; i++) {
				conn.createStatement().execute(
						"INSERT INTO RESERVATION VALUES(" + (i + 1) + ", 'name_" + i + "',CURRENT_TIMESTAMP())");
			}
			System.out.println("Data created");
			
			ResultSet set = conn.createStatement().
					executeQuery("SELECT ID, NAME, DATEDIFF('MILLISECOND', DATE '1970-01-01', DATETIME) FROM RESERVATION;");
			while(set.next()) {
				System.out.println(set.getInt(1)+" "+set.getString(2)+" "+set.getLong(3));
			}

		} catch (Exception e) {
			System.err.println(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}

}
