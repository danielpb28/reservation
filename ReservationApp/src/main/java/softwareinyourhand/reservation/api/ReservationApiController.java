package softwareinyourhand.reservation.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import softwareinyourhand.reservation.model.Error;
import softwareinyourhand.reservation.model.Reservation;
import softwareinyourhand.reservation.model.Reservations;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import softwareinyourhand.reservation.service.*;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-05-02T16:17:09.181Z[GMT]")
@RestController
public class ReservationApiController implements ReservationApi {

    private static final Logger log = LoggerFactory.getLogger(ReservationApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public ReservationApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }
    
    @org.springframework.beans.factory.annotation.Autowired
    private ReservationService service;

    public ResponseEntity<Void> createReservation(@Parameter(in = ParameterIn.DEFAULT, description = "Create new reservation", required=true, schema=@Schema()) @Valid @RequestBody Reservation body) {
        String accept = request.getHeader("Accept");
        service.saveOrUpdate(body);
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    public ResponseEntity<Void> deleteReservationById(@Parameter(in = ParameterIn.PATH, description = "The id of the reservation to delete", required=true, schema=@Schema()) @PathVariable("reservationId") String reservationId) {
        String accept = request.getHeader("Accept");
        service.delete(Long.parseLong(reservationId));
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    public ResponseEntity<Reservations> listReservation(@Parameter(in = ParameterIn.QUERY, description = "How many items to return at one time (max 100)" ,schema=@Schema()) @Valid @RequestParam(value = "limit", required = false) Integer limit,@Parameter(in = ParameterIn.QUERY, description = "Set the index where it starts" ,schema=@Schema()) @Valid @RequestParam(value = "offset", required = false) Integer offset,@Parameter(in = ParameterIn.QUERY, description = "Look for a record by ID" ,schema=@Schema()) @Valid @RequestParam(value = "search", required = false) String search) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
            	Reservations reservations = new Reservations();
            	reservations.addAll(service.getAllReservations());
                return new ResponseEntity<Reservations>(reservations, HttpStatus.OK);
            } catch (Exception e) {
                return new ResponseEntity<Reservations>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<Reservations>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Reservation> showReservationById(@Parameter(in = ParameterIn.PATH, description = "The id of the pet to retrieve", required=true, schema=@Schema()) @PathVariable("reservationId") String reservationId) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<Reservation>(service.getReservationById(Long.parseLong(reservationId)), HttpStatus.OK);
            } catch (Exception e) {
                return new ResponseEntity<Reservation>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<Reservation>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> updateReservationById(@Parameter(in = ParameterIn.PATH, description = "The id of the reservation to update", required=true, schema=@Schema()) @PathVariable("reservationId") String reservationId,@Parameter(in = ParameterIn.DEFAULT, description = "", required=true, schema=@Schema()) @Valid @RequestBody Reservation body) {
        String accept = request.getHeader("Accept");
        service.saveOrUpdate(body);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

}
