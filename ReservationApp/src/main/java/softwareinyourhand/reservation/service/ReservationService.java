package softwareinyourhand.reservation.service;

import java.util.ArrayList;  
import java.util.List;  
import org.springframework.beans.factory.annotation.Autowired;  
import org.springframework.stereotype.Service; 
import softwareinyourhand.reservation.jpa.*;

import softwareinyourhand.reservation.model.*;

@Service
public class ReservationService {
	@Autowired  
	ReservationRepository reservationRepository;  
	//getting all books record by using the method findaAll() of CrudRepository  
	public List<Reservation> getAllReservations()   
	{  
	List<Reservation> reservations = new ArrayList<Reservation>();  
	reservationRepository.findAll().forEach(reservation -> reservations.add(reservation));  
	return reservations;  
	}  
	//getting a specific record by using the method findById() of CrudRepository  
	public Reservation getReservationById(long id)   
	{  
	return reservationRepository.findById(id).get();  
	}  
	//saving a specific record by using the method save() of CrudRepository  
	public void saveOrUpdate(Reservation reservation)   
	{  
		reservationRepository.save(reservation);  
	}  
	//deleting a specific record by using the method deleteById() of CrudRepository  
	public void delete(long id)   
	{  
		reservationRepository.deleteById(id);  
	}  
	//updating a record  
	public void update(Reservation reservation, int reservationId)   
	{  
		reservationRepository.save(reservation);  
	}  
}
