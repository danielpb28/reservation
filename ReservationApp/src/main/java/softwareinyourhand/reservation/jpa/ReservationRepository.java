package softwareinyourhand.reservation.jpa;
 
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import softwareinyourhand.reservation.model.*;  

@Repository
public interface  ReservationRepository extends CrudRepository<Reservation, Long> {

}
